view: claims_claim_journal_entry {
  sql_table_name: public.journal_entry ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: claim_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.claim_id ;;
  }

  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }

  dimension_group: time {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.time ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  measure: count {
    type: count
    drill_fields: [id, claim.product_name, claim.lodgement_agent_username, claim.claimant_name, claim.id]
  }
}
