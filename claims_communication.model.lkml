connection: "prod-read-replica-claims_communication"

# include all the views
include: "*.view"

datagroup: rr_claims_communication_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: rr_claims_communication_default_datagroup

explore: claims_communication_call_survey {
# hidden: yes
  join: claims_communication_phone_call {
    type: left_outer
    sql_on: ${claims_communication_call_survey.phone_call_id} = ${claims_communication_phone_call.id} ;;
    relationship: many_to_one
  }
}

explore: claims_communication_conversation {
# hidden: yes
}

explore: claims_communication_email_blacklist {
# hidden: yes
}

explore: claims_communication_email_event {
# hidden: yes
}

explore: claims_communication_email_message {
# hidden: yes
  join: claims_communication_conversation {
    type: left_outer
    sql_on: ${claims_communication_email_message.conversation_id} = ${claims_communication_conversation.id} ;;
    relationship: many_to_one
  }
}

explore: claims_communication_emails_sent {
# hidden: yes
}

explore: claims_communication_phone_call {
# hidden: yes
}

explore: claims_communication_phone_call_note {
# hidden: yes
  join: claims_communication_phone_call {
    type: left_outer
    sql_on: ${claims_communication_phone_call_note.phone_call_id} = ${claims_communication_phone_call.id} ;;
    relationship: many_to_one
  }
}

explore: claims_communication_postal {
# hidden: yes
}

explore: claims_communication_schema_email_version {
# hidden: yes
}

explore: claims_communication_schema_phone_version {
# hidden: yes
}

explore: claims_communication_schema_postal_version {
# hidden: yes
}

explore: claims_communication_twilio_error_log {
# hidden: yes
}

explore: claims_communication_twilio_leg {
# hidden: yes
  join: claims_communication_phone_call {
    type: left_outer
    sql_on: ${claims_communication_twilio_leg.phone_call_id} = ${claims_communication_phone_call.id} ;;
    relationship: many_to_one
  }
}

explore: claims_communication_twilio_log {
# hidden: yes
  join: claims_communication_phone_call {
    type: left_outer
    sql_on: ${claims_communication_twilio_log.phone_call_id} = ${claims_communication_phone_call.id} ;;
    relationship: many_to_one
  }
}
