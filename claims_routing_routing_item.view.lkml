view: claims_routing_routing_item {
  sql_table_name: public.routing_item ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: confirmation_needed {
    type: yesno
    sql: ${TABLE}.confirmation_needed ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: force_assign_to_username {
    type: string
    sql: ${TABLE}.force_assign_to_username ;;
  }

  dimension: routing_type {
    type: string
    sql: ${TABLE}.routing_type ;;
  }

  dimension: selected_username {
    type: string
    sql: ${TABLE}.selected_username ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: task_id {
    type: number
    sql: ${TABLE}.task_id ;;
  }

  dimension: task_type {
    type: string
    sql: ${TABLE}.task_type ;;
  }

  measure: count {
    type: count
    drill_fields: [id, selected_username, force_assign_to_username, routing_confirm_request.count]
  }
}
