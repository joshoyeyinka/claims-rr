view: claims_claim_claim {
  sql_table_name: public.claim ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: claim_reference {
    type: string
    sql: ${TABLE}.claim_reference ;;
  }

  dimension: claimant_name {
    type: string
    sql: ${TABLE}.claimant_name ;;
  }

  dimension: credit_card_company {
    type: string
    sql: ${TABLE}.credit_card_company ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: customer_bank_details {
    type: string
    sql: ${TABLE}.customer_bank_details ;;
  }

  dimension: estimate {
    type: number
    sql: ${TABLE}.estimate ;;
  }

  dimension: external_reference {
    type: string
    sql: ${TABLE}.external_reference ;;
  }

  dimension: flags {
    type: string
    sql: ${TABLE}.flags ;;
  }

  dimension: incident_benefit_code {
    type: string
    sql: ${TABLE}.incident_benefit_code ;;
  }

  dimension_group: incident {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.incident_date ;;
  }

  dimension: incident_description {
    type: string
    sql: ${TABLE}.incident_description ;;
  }

  dimension: incident_id {
    type: number
    sql: ${TABLE}.incident_id ;;
  }

  dimension: incident_location {
    type: string
    sql: ${TABLE}.incident_location ;;
  }

  dimension: incident_travellers {
    type: string
    sql: ${TABLE}.incident_travellers ;;
  }

  dimension: interactor_id {
    type: number
    sql: ${TABLE}.interactor_id ;;
  }

  dimension: international_customer_bank_details {
    type: string
    sql: ${TABLE}.international_customer_bank_details ;;
  }

  dimension: lodgement_agent_username {
    type: string
    sql: ${TABLE}.lodgement_agent_username ;;
  }

  dimension: lodgement_channel {
    type: string
    sql: ${TABLE}.lodgement_channel ;;
  }

  dimension_group: lodgement {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.lodgement_date ;;
  }

  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }

  dimension: payee {
    type: string
    sql: ${TABLE}.payee ;;
  }

  dimension: policy_id {
    type: string
    sql: ${TABLE}.policy_id ;;
  }

  dimension: policy_number {
    type: string
    sql: ${TABLE}.policy_number ;;
  }

  dimension_group: policy_purchase {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.policy_purchase_time ;;
  }

  dimension: policy_schedule {
    type: string
    sql: ${TABLE}.policy_schedule ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: purpose_travel {
    type: string
    sql: ${TABLE}.purpose_travel ;;
  }

  dimension: removed {
    type: yesno
    sql: ${TABLE}.removed ;;
  }

  dimension: representative {
    type: string
    sql: ${TABLE}.representative ;;
  }

  dimension: stage {
    type: string
    sql: ${TABLE}.stage ;;
  }

  measure: businessdaysfromlodgeddate {
    type:  number
    sql: DATEDIFF('day', ${lodgement_date}, now()) - (FLOOR(DATEDIFF('day', ${lodgement_date}, now()) / 7) * 2) +
          (CASE WHEN DATE_PART(dow, ${lodgement_date}) - DATE_PART(dow, now()) IN (1, 2, 3, 4, 5) AND DATE_PART(dow, now()) != 0
          THEN 2 ELSE 0 END) +
          (CASE WHEN DATE_PART(dow, ${lodgement_date}) != 0 AND DATE_PART(dow, now()) = 0
          THEN 1 ELSE 0 END) +
          (CASE WHEN DATE_PART(dow, ${lodgement_date}) = 0 AND DATE_PART(dow, now()) != 0
          THEN 1 ELSE 0 END);;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      product_name,
      lodgement_agent_username,
      claimant_name,
      accounting_record.count,
      claim_attachment.count,
      claim_expense.count,
      claim_payee.count,
      journal_entry.count,
      online_claim_form.count,
      settlement.count
    ]
  }
}
