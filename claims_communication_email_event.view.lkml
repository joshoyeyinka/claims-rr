view: claims_communication_email_event {
  sql_table_name: public.email_event ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: email_id {
    type: number
    sql: ${TABLE}.email_id ;;
  }

  dimension: event {
    type: string
    sql: ${TABLE}.event ;;
  }

  dimension_group: time_event_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.time_event_received ;;
  }

  dimension: to_email {
    type: string
    sql: ${TABLE}.to_email ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
