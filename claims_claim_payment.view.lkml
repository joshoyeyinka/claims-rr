view: claims_claim_payment {
  sql_table_name: public.payment ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: excess {
    type: number
    sql: ${TABLE}.excess ;;
  }

  dimension: expenses {
    type: string
    sql: ${TABLE}.expenses ;;
  }

  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }

  dimension: normalized_total {
    type: number
    sql: ${TABLE}.normalized_total ;;
  }

  dimension: payee {
    type: string
    sql: ${TABLE}.payee ;;
  }

  dimension: reconciled_total {
    type: number
    sql: ${TABLE}.reconciled_total ;;
  }

  dimension: settlement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.settlement_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: total {
    type: number
    sql: ${TABLE}.total ;;
  }

  measure: count {
    type: count
    drill_fields: [id, settlement.id]
  }
}
