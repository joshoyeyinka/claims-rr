connection: "prod-read-replica-claims_claim"

# include all the views
include: "*.view"

datagroup: rr_claims_claim_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: rr_claims_claim_default_datagroup

explore: claims_claim_accounting_record {
# hidden: yes
  join: claims_claim_claim {
    type: left_outer
    sql_on: ${claims_claim_accounting_record.claim_id} = ${claims_claim_claim.id} ;;
    relationship: many_to_one
  }
}

explore: claims_claim_claim {
# hidden: yes
}

explore: claims_claim_claim_attachment {
# hidden: yes
  join: claims_claim_claim {
    type: left_outer
    sql_on: ${claims_claim_claim_attachment.claim_id} = ${claims_claim_claim.id} ;;
    relationship: many_to_one
  }
}

explore: claims_claim_claim_expense {
# hidden: yes
  join: claims_claim_claim {
    type: left_outer
    sql_on: ${claims_claim_claim_expense.claim_id} = ${claims_claim_claim.id} ;;
    relationship: many_to_one
  }

  join: claims_claim_settlement {
    type: left_outer
    sql_on: ${claims_claim_claim_expense.settlement_id} = ${claims_claim_settlement.id} ;;
    relationship: many_to_one
  }
}

explore: claims_claim_claim_payee {
# hidden: yes
  join: claims_claim_claim {
    type: left_outer
    sql_on: ${claims_claim_claim_payee.claim_id} = ${claims_claim_claim.id} ;;
    relationship: many_to_one
  }
}

explore: claims_claim_currency_conversion {
# hidden: yes
}

explore: claims_claim_currency_pair {
# hidden: yes
}

explore: claims_claim_invoice {
# hidden: yes
}

explore: claims_claim_journal_entry {
# hidden: yes
  join: claims_claim_claim {
    type: left_outer
    sql_on: ${claims_claim_journal_entry.claim_id} = ${claims_claim_claim.id} ;;
    relationship: many_to_one
  }
}

explore: claims_claim_natural_disaster_event {
# hidden: yes
}

explore: claims_claim_online_claim_form {
# hidden: yes
  join: claims_claim_claim {
    type: left_outer
    sql_on: ${claims_claim_online_claim_form.claim_id} = ${claims_claim_claim.id} ;;
    relationship: many_to_one
  }
}

explore: claims_claim_payment {
# hidden: yes
  join: claims_claim_settlement {
    type: left_outer
    sql_on: ${claims_claim_payment.settlement_id} = ${claims_claim_settlement.id} ;;
    relationship: many_to_one
  }

  join: claims_claim_claim {
    type: left_outer
    sql_on: ${claims_claim_settlement.claim_id} = ${claims_claim_claim.id} ;;
    relationship: many_to_one
  }
}

explore: claims_claim_schema_claim_version {
# hidden: yes
}

explore: claims_claim_schema_currency_version {
# hidden: yes
}

explore: claims_claim_settlement {
# hidden: yes
  join: claims_claim_claim {
    type: left_outer
    sql_on: ${claims_claim_settlement.claim_id} = ${claims_claim_claim.id} ;;
    relationship: many_to_one
  }
}
