view: claims_claim_online_claim_form {
  sql_table_name: public.online_claim_form ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: attachments {
    type: string
    sql: ${TABLE}.attachments ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: claim_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.claim_id ;;
  }

  dimension: claim_reference {
    type: string
    sql: ${TABLE}.claim_reference ;;
  }

  dimension: claimant_name {
    type: string
    sql: ${TABLE}.claimant_name ;;
  }

  dimension: customer_bank_details {
    type: string
    sql: ${TABLE}.customer_bank_details ;;
  }

  dimension: estimate {
    type: number
    sql: ${TABLE}.estimate ;;
  }

  dimension: expenses {
    type: string
    sql: ${TABLE}.expenses ;;
  }

  dimension: incident {
    type: string
    sql: ${TABLE}.incident ;;
  }

  dimension: interactor_id {
    type: number
    sql: ${TABLE}.interactor_id ;;
  }

  dimension: international_customer_bank_details {
    type: string
    sql: ${TABLE}.international_customer_bank_details ;;
  }

  dimension_group: lodgement {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.lodgement_date ;;
  }

  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }

  dimension: policy_id {
    type: string
    sql: ${TABLE}.policy_id ;;
  }

  dimension: policy_schedule_id {
    type: string
    sql: ${TABLE}.policy_schedule_id ;;
  }

  dimension: purpose_travel {
    type: string
    sql: ${TABLE}.purpose_travel ;;
  }

  dimension: representative {
    type: string
    sql: ${TABLE}.representative ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      claimant_name,
      claim.product_name,
      claim.lodgement_agent_username,
      claim.claimant_name,
      claim.id
    ]
  }
}
