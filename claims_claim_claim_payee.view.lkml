view: claims_claim_claim_payee {
  sql_table_name: public.claim_payee ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: approved {
    type: yesno
    sql: ${TABLE}.approved ;;
  }

  dimension: approved_by {
    type: string
    sql: ${TABLE}.approved_by ;;
  }

  dimension: bank_details {
    type: string
    sql: ${TABLE}.bank_details ;;
  }

  dimension: claim_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.claim_id ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.created_by ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: payee_type {
    type: string
    sql: ${TABLE}.payee_type ;;
  }

  dimension: remittance_email {
    type: string
    sql: ${TABLE}.remittance_email ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      claim.product_name,
      claim.lodgement_agent_username,
      claim.claimant_name,
      claim.id
    ]
  }
}
