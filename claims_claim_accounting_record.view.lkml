view: claims_claim_accounting_record {
  sql_table_name: public.accounting_record ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: accountable_class {
    type: string
    sql: ${TABLE}.accountable_class ;;
  }

  dimension: accountable_id {
    type: number
    sql: ${TABLE}.accountable_id ;;
  }

  dimension_group: accounting {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.accounting_date ;;
  }

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension: claim_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.claim_id ;;
  }

  dimension_group: creation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creation_date ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: ledger {
    type: string
    sql: ${TABLE}.ledger ;;
  }

  measure: count {
    type: count
    drill_fields: [id, claim.product_name, claim.lodgement_agent_username, claim.claimant_name, claim.id]
  }
}
