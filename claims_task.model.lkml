connection: "prod-read-replica-claims_task"

# include all the views
include: "*.view"

datagroup: rr_claims_task_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: rr_claims_task_default_datagroup

explore: claims_task_schema_task_version {
# hidden: yes
}

explore: claims_task_task {
# hidden: yes
}

explore: claims_task_task_linked_entity {
# hidden: yes
  join: claims_task_task {
    type: left_outer
    sql_on: ${claims_task_task_linked_entity.task_id} = ${claims_task_task.id} ;;
    relationship: many_to_one
  }
}

explore: claims_task_task_suggested_entity {
# hidden: yes
  join: claims_task_task {
    type: left_outer
    sql_on: ${claims_task_task_suggested_entity.task_id} = ${claims_task_task.id} ;;
    relationship: many_to_one
  }
}
