view: claims_claim_claim_expense {
  sql_table_name: public.claim_expense ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: approved_amount {
    type: number
    sql: ${TABLE}.approved_amount ;;
  }

  dimension: asset_age {
    type: number
    sql: ${TABLE}.asset_age ;;
  }

  dimension: benefit_code {
    type: string
    sql: ${TABLE}.benefit_code ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: claim_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.claim_id ;;
  }

  dimension: claimed_amount {
    type: number
    sql: ${TABLE}.claimed_amount ;;
  }

  dimension: claimed_amount_converted {
    type: number
    sql: ${TABLE}.claimed_amount_converted ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: depreciation_amount {
    type: number
    sql: ${TABLE}.depreciation_amount ;;
  }

  dimension: depreciation_applicable {
    type: yesno
    sql: ${TABLE}.depreciation_applicable ;;
  }

  dimension_group: expense {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.expense_date ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: normalized_approved_amount {
    type: number
    sql: ${TABLE}.normalized_approved_amount ;;
  }

  dimension: note {
    type: string
    sql: ${TABLE}.note ;;
  }

  dimension: on_settlement_sheet {
    type: yesno
    sql: ${TABLE}.on_settlement_sheet ;;
  }

  dimension: override_approved_amount {
    type: yesno
    sql: ${TABLE}.override_approved_amount ;;
  }

  dimension: payee_reference_id {
    type: number
    sql: ${TABLE}.payee_reference_id ;;
  }

  dimension: payment_currency {
    type: string
    sql: ${TABLE}.payment_currency ;;
  }

  dimension: removed {
    type: yesno
    sql: ${TABLE}.removed ;;
  }

  dimension: settlement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.settlement_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: traveller_ids {
    type: string
    sql: ${TABLE}.traveller_ids ;;
  }

  measure: averageapprovedamount {
    type: average
    sql: ${approved_amount} ;;
    value_format: "#;(#)"
  }

  measure: averageclaimedamount {
    type: average
    sql:  ${claimed_amount} ;;
    value_format: "#;(#)"
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      claim.product_name,
      claim.lodgement_agent_username,
      claim.claimant_name,
      claim.id,
      settlement.id
    ]
  }
}
