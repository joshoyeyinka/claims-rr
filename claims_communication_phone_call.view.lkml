view: claims_communication_phone_call {
  sql_table_name: public.phone_call ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: agent_transfer_call_sid {
    type: string
    sql: ${TABLE}.agent_transfer_call_sid ;;
  }

  dimension: call_sid {
    type: string
    sql: ${TABLE}.call_sid ;;
  }

  dimension: client {
    type: string
    sql: ${TABLE}.client ;;
  }

  dimension: conference_sid {
    type: string
    sql: ${TABLE}.conference_sid ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension_group: current_hold_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.current_hold_end_time ;;
  }

  dimension_group: current_hold_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.current_hold_start_time ;;
  }

  dimension: direction {
    type: string
    sql: ${TABLE}.direction ;;
  }

  dimension: dtype {
    type: string
    sql: ${TABLE}.dtype ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_time ;;
  }

  dimension: external_call_transfer_sid {
    type: string
    sql: ${TABLE}.external_call_transfer_sid ;;
  }

  dimension: forwarded_from_number {
    type: string
    sql: ${TABLE}.forwarded_from_number ;;
  }

  dimension: from_number {
    type: string
    sql: ${TABLE}.from_number ;;
  }

  dimension_group: ghost_termination {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ghost_termination_time ;;
  }

  dimension: recording_ref {
    type: string
    sql: ${TABLE}.recording_ref ;;
  }

  dimension: recording_saved {
    type: yesno
    sql: ${TABLE}.recording_saved ;;
  }

  dimension: recording_sid {
    type: string
    sql: ${TABLE}.recording_sid ;;
  }

  dimension: recording_status {
    type: string
    sql: ${TABLE}.recording_status ;;
  }

  dimension: related_phone_call {
    type: number
    sql: ${TABLE}.related_phone_call ;;
  }

  dimension: routing_messages {
    type: string
    sql: ${TABLE}.routing_messages ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}.tags ;;
  }

  dimension: to_admin_browser_id {
    type: string
    sql: ${TABLE}.to_admin_browser_id ;;
  }

  dimension: to_admin_username {
    type: string
    sql: ${TABLE}.to_admin_username ;;
  }

  dimension: to_number {
    type: string
    sql: ${TABLE}.to_number ;;
  }

  dimension: total_hold_time_seconds {
    type: number
    sql: ${TABLE}.total_hold_time_seconds ;;
  }

  dimension: transfer_phone_number {
    type: string
    sql: ${TABLE}.transfer_phone_number ;;
  }

  dimension: transfer_type {
    type: string
    sql: ${TABLE}.transfer_type ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      to_admin_username,
      call_survey.count,
      phone_call_note.count,
      twilio_leg.count,
      twilio_log.count
    ]
  }
}
