connection: "prod-read-replica-claims_auth"

# include all the views
include: "*.view"

datagroup: rr_claims_auth_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: rr_claims_auth_default_datagroup

explore: claims_auth_auth {
# hidden: yes
}

explore: claims_auth_break {
# hidden: yes
  join: claims_auth_break_type {
    type: left_outer
    sql_on: ${claims_auth_break.break_type_id} = ${claims_auth_break_type.id} ;;
    relationship: many_to_one
  }
}

explore: claims_auth_break_type {
# hidden: yes
}

explore: claims_auth_schema_auth_version {
# hidden: yes
}

explore: claims_auth_web_socket_session_id {
# hidden: yes
}
