view: claims_claim_settlement {
  sql_table_name: public.settlement ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: approved {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.approved ;;
  }

  dimension: approved_by {
    type: string
    sql: ${TABLE}.approved_by ;;
  }

  dimension: claim_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.claim_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: excess {
    type: number
    sql: ${TABLE}.excess ;;
  }

  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }

  dimension: settlement_sheet {
    type: string
    sql: ${TABLE}.settlement_sheet ;;
  }

  dimension: statement {
    type: string
    sql: ${TABLE}.statement ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: submitted_by {
    type: string
    sql: ${TABLE}.submitted_by ;;
  }

  measure: dayscreatedtoapproved {
    type: number
    sql: CASE
              WHEN sum(${approved_date} - ${created_date}) IS NULL THEN 0
              ELSE sum(${approved_date} - ${created_date}) END;;
  }

  measure: noofsettled {
    type: number
    sql: CASE
              WHEN ${status} = 'Settled' THEN 1
              ELSE 0 END;;
  }

  measure: noofnonsettled{
    type: number
    sql:  CASE
              WHEN ${status} = 'Settled' THEN 0
              ELSE 1 END;;
  }

  measure: percentageofsettled {
    type:  number
    sql: ${noofsettled}/${noofnonsettled} ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      claim.product_name,
      claim.lodgement_agent_username,
      claim.claimant_name,
      claim.id,
      claim_expense.count,
      payment.count
    ]
  }
}
