view: claims_claim_currency_pair {
  sql_table_name: public.currency_pair ;;

  dimension: base {
    type: string
    sql: ${TABLE}.base ;;
  }

  dimension_group: offer {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.offer ;;
  }

  dimension: provider {
    type: string
    sql: ${TABLE}.provider ;;
  }

  dimension: quote {
    type: string
    sql: ${TABLE}.quote ;;
  }

  dimension: rate {
    type: number
    sql: ${TABLE}.rate ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
