view: claims_communication_call_survey {
  sql_table_name: public.call_survey ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: message {
    type: string
    sql: ${TABLE}.message ;;
  }

  dimension: option {
    type: string
    sql: ${TABLE}.option ;;
  }

  dimension: phone_call_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.phone_call_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, phone_call.to_admin_username, phone_call.id]
  }
}
