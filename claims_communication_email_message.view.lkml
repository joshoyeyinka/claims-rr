view: claims_communication_email_message {
  sql_table_name: public.email_message ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: adminusername {
    type: string
    sql: ${TABLE}.adminusername ;;
  }

  dimension: attachments {
    type: string
    sql: ${TABLE}.attachments ;;
  }

  dimension: conversation_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.conversation_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: dtype {
    type: string
    sql: ${TABLE}.dtype ;;
  }

  dimension: email_replying_to {
    type: number
    sql: ${TABLE}.email_replying_to ;;
  }

  dimension: er_bcc {
    type: string
    sql: ${TABLE}.er_bcc ;;
  }

  dimension: er_cc {
    type: string
    sql: ${TABLE}.er_cc ;;
  }

  dimension: er_from {
    type: string
    sql: ${TABLE}.er_from ;;
  }

  dimension: er_to {
    type: string
    sql: ${TABLE}.er_to ;;
  }

  dimension: folder {
    type: string
    sql: ${TABLE}.folder ;;
  }

  dimension: html {
    type: string
    sql: ${TABLE}.html ;;
  }

  dimension: mailbox {
    type: string
    sql: ${TABLE}.mailbox ;;
  }

  dimension: plain {
    type: string
    sql: ${TABLE}.plain ;;
  }

  dimension_group: processed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.processed ;;
  }

  dimension: representative {
    type: string
    sql: ${TABLE}.representative ;;
  }

  dimension_group: sent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sent ;;
  }

  dimension: subject {
    type: string
    sql: ${TABLE}.subject ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}.tags ;;
  }

  dimension: uid {
    type: string
    sql: ${TABLE}.uid ;;
  }

  measure: count {
    type: count
    drill_fields: [id, adminusername, conversation.id]
  }
}
