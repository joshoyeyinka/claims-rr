view: claims_claim_currency_conversion {
  sql_table_name: public.currency_conversion ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: base {
    type: string
    sql: ${TABLE}.base ;;
  }

  dimension_group: exchange {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.exchange_date ;;
  }

  dimension_group: loaded {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.loaded_date ;;
  }

  dimension: rates {
    type: string
    sql: ${TABLE}.rates ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
