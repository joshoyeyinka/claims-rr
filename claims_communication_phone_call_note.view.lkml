view: claims_communication_phone_call_note {
  sql_table_name: public.phone_call_note ;;

  dimension: note {
    type: string
    sql: ${TABLE}.note ;;
  }

  dimension: phone_call_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.phone_call_id ;;
  }

  measure: count {
    type: count
    drill_fields: [phone_call.to_admin_username, phone_call.id]
  }
}
