view: claims_claim_invoice {
  sql_table_name: public.invoice ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: expense_id {
    type: number
    sql: ${TABLE}.expense_id ;;
  }

  dimension: external_reference {
    type: string
    sql: ${TABLE}.external_reference ;;
  }

  dimension: file_ref {
    type: string
    sql: ${TABLE}.file_ref ;;
  }

  dimension: invoice_number {
    type: string
    sql: ${TABLE}.invoice_number ;;
  }

  dimension: payee_id {
    type: number
    sql: ${TABLE}.payee_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
