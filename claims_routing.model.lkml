connection: "prod-read-replica-claims_routing"

# include all the views
include: "*.view"

datagroup: rr_claims_routing_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: rr_claims_routing_default_datagroup

explore: claims_routing_routing_confirm_request {
# hidden: yes
  join: claims_routing_routing_item {
    type: left_outer
    sql_on: ${claims_routing_routing_confirm_request.routing_item_id} = ${claims_routing_routing_item.id} ;;
    relationship: many_to_one
  }
}

explore: claims_routing_routing_item {
# hidden: yes
}

explore: claims_routing_schema_routing_version {
# hidden: yes
}
