view: claims_auth_auth {
  sql_table_name: public.auth ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: apps {
    type: string
    sql: ${TABLE}.apps ;;
  }

  dimension: authorities {
    type: string
    sql: ${TABLE}.authorities ;;
  }

  dimension: enabled {
    type: yesno
    sql: ${TABLE}.enabled ;;
  }

  dimension: enabled_tasks {
    type: string
    sql: ${TABLE}.enabled_tasks ;;
  }

  dimension_group: last_http_heartbeat {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_http_heartbeat ;;
  }

  dimension_group: last_ws_heartbeat {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_ws_heartbeat ;;
  }

  dimension: locations {
    type: string
    sql: ${TABLE}.locations ;;
  }

  dimension: logins {
    type: string
    sql: ${TABLE}.logins ;;
  }

  dimension: payment_approval_limit {
    type: number
    sql: ${TABLE}.payment_approval_limit ;;
  }

  dimension: self_approval_limit {
    type: number
    sql: ${TABLE}.self_approval_limit ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: teams {
    type: string
    sql: ${TABLE}.teams ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  measure: countofenabledclaims {
    sql: SUM(CASE
               WHEN ${TABLE}.enabled = 'Yes' THEN 1
               WHEN ${TABLE}.enabled = 'No' THEN 0
               ELSE NUll
        END);;
  }

  measure: countofrejectedclaims {
    sql: SUM(CASE
               WHEN ${TABLE}.enabled = 'Yes' THEN 0
               WHEN ${TABLE}.enabled = 'No' THEN 1
               ELSE NUll
        END);;
  }

  measure: percentageofenabledclaims {
    type: number
    sql:  ${countofenabledclaims} / (${countofenabledclaims} + ${countofrejectedclaims}) ;;
  }

  measure: count {
    type: count
    drill_fields: [id, username]
  }
}
