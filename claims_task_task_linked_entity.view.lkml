view: claims_task_task_linked_entity {
  sql_table_name: public.task_linked_entity ;;

  dimension: entity_ref {
    type: string
    sql: ${TABLE}.entity_ref ;;
  }

  dimension: task_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.task_id ;;
  }

  measure: count {
    type: count
    drill_fields: [task.id, task.assigned_username]
  }
}
