view: claims_communication_twilio_log {
  sql_table_name: public.twilio_log ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: call_sid {
    type: string
    sql: ${TABLE}.call_sid ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: log_code {
    type: string
    sql: ${TABLE}.log_code ;;
  }

  dimension: message {
    type: string
    sql: ${TABLE}.message ;;
  }

  dimension: phone_call_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.phone_call_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: twilio_state {
    type: string
    sql: ${TABLE}.twilio_state ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  measure: count {
    type: count
    drill_fields: [id, username, phone_call.to_admin_username, phone_call.id]
  }
}
