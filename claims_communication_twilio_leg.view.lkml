view: claims_communication_twilio_leg {
  sql_table_name: public.twilio_leg ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: browser_id {
    type: string
    sql: ${TABLE}.browser_id ;;
  }

  dimension: call_duration {
    type: number
    sql: ${TABLE}.call_duration ;;
  }

  dimension: call_sid {
    type: string
    sql: ${TABLE}.call_sid ;;
  }

  dimension: caller_name {
    type: string
    sql: ${TABLE}.caller_name ;;
  }

  dimension: conference_id {
    type: string
    sql: ${TABLE}.conference_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: on_hold_duration {
    type: number
    sql: ${TABLE}.on_hold_duration ;;
  }

  dimension: phone_call_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.phone_call_id ;;
  }

  dimension: phone_number {
    type: string
    sql: ${TABLE}.phone_number ;;
  }

  dimension: twilio_leg_status {
    type: string
    sql: ${TABLE}.twilio_leg_status ;;
  }

  dimension: twilio_leg_type {
    type: string
    sql: ${TABLE}.twilio_leg_type ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated ;;
  }

  measure: total_leg_duration {
    type: sum
    sql_distinct_key: ${id} ;;
    sql: ${call_duration} ;;
    value_format: "0"
  }

  measure: average_call_duration {
    type: average
    sql_distinct_key: ${phone_call_id} ;;
    sql: ${call_duration} ;;
    value_format: "0"
  }

  measure: count_phone_call {
    type: count_distinct
    sql: ${phone_call_id} ;;
    value_format: "0"
  }

  measure: count {
    type: count
    drill_fields: [id, caller_name, phone_call.to_admin_username, phone_call.id]
  }
}
